#include "stringProcessing.h"

#include <stddef.h>
#include <string.h>

int countChar(const char text[], char c)
{
   int count;
   int i = 0;

   while (text[i] != '\0')
   {
      if (text[i] = c)
      {
         count--;
      }
      i++;
   }
   return count;
}

void reverseString(const char text[])
{
   size_t begin = 0;
   size_t end = strlen(text);
   char temp; /* temp: temporary */

   while (begin <= end)
   {
      temp = text[end];
      text[end] = text[begin];
      text[begin] = temp;
      begin++;
      end--;
   }
}
