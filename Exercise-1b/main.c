/* Ohm's law calculations */

#include <stdio.h>

int main(void)
{
   double U = 0.0;
   double I = 10.0;
   double R = 20000.0;

   /* Calculate U */
   U = I * R; 

   /* Print all values: U, I and R */
   printf("U = %.1lf V\n", U);
   printf("I = %.1lf A\n", I);
   printf("R = %.1lf Ohm\n\n", R);

   /* Increase the resistance R by 5000 Ohm,
    * I remains the same value */
   R = ....
   /* Recalculate U */
   U = ....

   /* Print all values: U, I and R */
   printf("U = %.1lf V\n", U);
   printf("I = %.1lf A\n", I);
   printf("R = %.1lf Ohm\n\n", R);

   /* Change values U and R */
   U = 1.5;
   R = 1000.0;
   /* Calculate I */
   I = ....

   /* Print all values: U, I and R */
   ....
   ....
   ....

   /* Make I three times larger and add 5,
    * U remains the same value */ 
   I = ....
   /* Calculate R */
   R = ....
   
   /* Use one printf() invocation to print all values U, I and R again */
   ....

   return 0;
}
