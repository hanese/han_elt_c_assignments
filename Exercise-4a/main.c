/* Fahrenheit and Celsius table */

#include <stdio.h>

#define MAX_LOOPCOUNT 15

int main()
{
   double stepFahrenheit = 2.0;
   double Fahrenheit = 10.0;
   double Celsius = 0.0;

   printf(
      "- Table temperature conversions -\n\n"
      "%12s %12s\n\n",
      "Fahrenheit", "Celsius");

   /* Always use int for a loop counting variable */
   for (int i = 0; i < MAX_LOOPCOUNT; i++)
   {
      Fahrenheit += (i * stepFahrenheit);
      Celsius = ....

      /* Print Fahrenheit and Celsius */
      printf(.....);
   }

   return 0;
}
