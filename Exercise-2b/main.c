/* Reading a key pressed, if-else statement */

#include <stdio.h>

int main()
{
   char c;

   printf("Enter character: ");
   /* Use a a space befor %c for ignoring the <Enter> key */
   scanf(" %c", &c);

   if ((c == 'a') || (c == 'b') ....)
   {
      ....   
   }
   else
   {
      if (....)
      {
         ....
         ....
      }
   }

   ....
   

   return 0;
}
