/* The printf function and strings */

#include <stdio.h>

int main(void)
{
   printf("Hello World C programming\n");
   
   // printf(....);

   // printf("Integer value = %d\nfloating point value = % lf\n ", ....);

   return 0;
}
