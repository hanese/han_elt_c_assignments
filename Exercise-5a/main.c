#include <stdio.h>

typedef struct {
   int number;
   char twoChars[3];
} postalcode_t;

#define NUMBER_OF_PCS 3

int main()
{
   postalcode_t pcs1[NUMBER_OF_PCS] = {
      {1234, "AB"}, .... };

   /* Array intialised with all 0's */
   postalcode_t pcs2[NUMBER_OF_PCS] = {0}; 

   /* Print the size of the struct in bytes */
   printf(....);

   /* Compare pcs1[0] and pcs1[1] and print appropriate texts */
   if (....)
   {
      printf(....);
   }
   else 
   {
      printf(....);
   }

   /* Compare pcs1[0] and pcs1[2] and print appropriate texts */
   if (....)
   {
       ....
   }
   else
   {
      ....
   }
   
   /* Write the content of the array pcs to a text file */
   FILE *pcsFile = fopen(....);

   for (....)
   {
      ....
   }
   fclose(....);

   /* Read the text file to pcs2 array and print contents */
   pcsFile = fopen(....);

   for (....)
   {
      ....
      ....
   }
   fclose(....);

   return 0;
}
