#ifndef FCCONVERSION_H
#define FCCONVERSION_H

double FtoC(double fahrenheit);
double CtoF(double celcius);

#endif
