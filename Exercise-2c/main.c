/* Reading a key pressed, switch-case statement */

#include <stdio.h>

int main()
{
   char c;

   printf("Enter character: ");
   /* Use a a space befor %c for ignoring the <Enter> key */
   scanf(" %c", &c);

   switch (c)
   {
      case 'w':
        ....
        break; 
      case 'a':
        ....` 
 

      default:
         ....
   }
   
   return 0;
}
