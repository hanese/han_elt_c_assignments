/* Use of an int arrays */

#include <stdio.h>

#define SIZE_data1 4
#define SIZE_data2 10

int main()
{
   int data1[SIZE_data1] = ....
   int sum = 0;
   int data2[....] = ....

   /* Enter all elements in data1 array in a loop */
   for (....) 
   {
      int input;

      printf(....);
      scanf(....);
      /* Remove al remaining buffered chars in stdin */
      while (getchar() != '\n')
      {
         /* Empty loop */
      }
     .... 
   }

   /* Calclulate sum of all elements in data1 in a loop */
   ....
   ....

   /* Enter all values for data2 in a loop */
   ....

   ....
   
   return 0;
}
