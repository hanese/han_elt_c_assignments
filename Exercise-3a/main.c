/* Strings, char arrays */

#include <ctype.h>
#include <stdio.h>
#include <string.h>

#define SIZE 30

int main(void)
{
   /* String: char array */
   char text[SIZE] = "Code number: 246810";
   int nDigits = 0;

   for (size_t i = 0; i < strlen(text); i++)
   {
      if (isdigit(text[i]))
      {
         ....
      }
   }

   /* Print number of digits */
   printf(....);

   /* Change contents of text string array */
   strcpy(....);

   /* Change all lower case characters to upper case */
   for (size_t i = 0; i < strlen(text); i++)
   {
      ....= toupper(....);
   }

   /* Print all characters and their hexadecimal value prefixed by 0x */
   for (size_t i = 0; i < strlen(text); i++)
   {
      printf(....);
   }

   return 0;
}
