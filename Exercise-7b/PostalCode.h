#ifndef POSTALCODE_H
#define POSTALCODE_H

typedef struct {
   int number;
   char twoChars[3];
} postalcode_t;

postalcode_t *searchPostalCodeUpperCaseChars(const postalcode_t pcs[],
                                             int size,
                                             const char uppercaseChars[]);

int writePostalCodes(const postalcode_t pcs[], int size,
                     const char filename[]);

int readPostalCodes(postalcode_t pcs[], int size, const char filename[]);

#endif
