/* Modular programming, module: PostalCode */

#include "PostalCode.h"

#include <stdio.h>

#define N_PCS 5

int main()
{
   postalcode_t data[N_PCS] = {....};

   postalcode_t *pPostalCode =
      searchPostalCodeUpperCaseChars(data, N_PCS, "AB");

   if (pPostalCode != NULL)
   {
      /* print the result */
      ....
   }
   else 
   {
      ....
   }


   return 0;
}
