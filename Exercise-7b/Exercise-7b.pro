TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
   PostalCode.c \
   main.c

HEADERS += \
   PostalCode.h
