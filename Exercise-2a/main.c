/* Fahrenheit and Celsius conversions */

#include <stdio.h>

int main()
{
   /* Defining declarations and initialisations */
   double Fahrenheit = 10.0;
   double Celsius = 0.0;

   /* Formula for converting Fahrenheit to Celsius */
   Celsius = 5.0 * (Fahrenheit - 32.0) / 9.0;
   /* Print values with two decimal places */
   printf("Fahrenheit = %.2lf ==> Celsius = %.2lf\n", Fahrenheit, Celsius);
   
   if (Celsius < 12.0)
   {
      printf("It is cold outside\n");
   }
   else
   {
      if (....)
      {
         ....
      }  
   }

   ....
   
   /* Formula for converting Celsius to Fahrenheit */


   /* Print values with one decimal place */


   return 0;
}
